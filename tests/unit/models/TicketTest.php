<?php
/**
 * Created by PhpStorm.
 * User: maxweb
 * Date: 28.08.2018
 * Time: 14:47
 */

namespace tests\models;


use app\models\Ticket;

class TicketTest extends \Codeception\Test\Unit
{
    private $model;

    public function testSeats(){

        $this->model = new Ticket([
            'seans_id' => 1, //Терминатор (добавлен в миграциях) (максимум 50 мест)
            'user_id' => 1, //client (добавлен в миграциях)
            'ticket_amount' => 100,
        ]);

        //валидация не пройдет из-за недостаточного количества мест
        expect_not(($this->model->validate()));

        //валидация пройдет
        $this->model->ticket_amount = 20;
        expect($this->model->validate());
    }

}