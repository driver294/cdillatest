<?php
/**
 * Created by PhpStorm.
 * User: maxweb
 * Date: 28.08.2018
 * Time: 11:00
 */
use yii\db\Migration;


/**
 * Class m180828_110011_create_user_table
 * Класс для создания таблицы пользователей
 */
class m180828_110011_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('user', [
            'user_id'=>$this->bigPrimaryKey()->notNull()->unsigned(),
            'user_name' => $this->string()->null(),
            'user_password' => $this->string()->notNull(),
            'auth_key' => $this->string()->notNull()->defaultValue(""),
        ], $tableOptions
        );

        $this->createIndex('idx-user-user_id','user','user_id');


        $this->insert('user', [
            'user_name' => 'client',
            'user_password' => 'client',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
