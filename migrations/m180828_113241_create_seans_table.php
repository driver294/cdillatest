<?php
/**
 * Created by PhpStorm.
 * User: maxweb
 * Date: 28.08.2018
 * Time: 11:10
 */
use yii\db\Migration;

/**
 * Class m180828_113241_create_seans_table
 * Класс для создания таблицы сеансов
 */
class m180828_113241_create_seans_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('seans', [
            'seans_id'=>$this->bigPrimaryKey()->notNull()->unsigned(),
            'seans_name'=>$this->string()->notNull(),
            'seans_showing_time'=>$this->dateTime()->notNull(),
            'seans_seats' => $this->integer()->notNull()->defaultValue(50),
        ], $tableOptions
        );

        $this->createIndex('idx-seans-seans_id','seans','seans_id');

        $this->insert('seans', [
            'seans_name' => 'Терминатор 10',
            'seans_showing_time' => '2018-09-01 15:30',
            'seans_seats' => 50
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('seans');
    }
}