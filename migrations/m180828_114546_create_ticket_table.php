<?php
/**
 * Created by PhpStorm.
 * User: maxweb
 * Date: 28.08.2018
 * Time: 12:05
 */
use yii\db\Migration;

/**
 * Class m180828_114546_create_ticket_table
 * Класс для создания таблицы билетов (ticket)
 */
class m180828_114546_create_ticket_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('ticket', [
            'ticket_id'=>$this->bigPrimaryKey()->notNull()->unsigned(),
            'seans_id'=>$this->bigInteger()->notNull()->unsigned(),     //связь с сеансом
            'user_id'=>$this->bigInteger()->notNull()->unsigned(),      //связь с пользователем
            'ticket_amount' => $this->string()->null(),                 //количество забронированных билетов
        ], $tableOptions
        );

        $this->createIndex('idx-ticket-ticket_id','ticket','ticket_id');
        $this->createIndex('idx-ticket-seans_id','ticket','seans_id');
        $this->createIndex('idx-ticket-user_id','ticket','user_id');

        $this->addForeignKey(
            'ticket_ibfk_1',
            'ticket',
            'seans_id',
            'seans',
            'seans_id',
            'CASCADE'
        );
        $this->addForeignKey(
            'ticket_ibfk_2',
            'ticket',
            'user_id',
            'user',
            'user_id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ticket');
    }
}