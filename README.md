<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Тестовое задание</h1>
    <br>
</p>



Структура приложения
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      migrations/         содержит файлы миграций
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



Требования
------------

Минимаьлные требования для этого проекта это веб-сервер с поддержкой PHP 5.6.0.


Установка
------------

### Установка с помощью Composer

Клонировать ветку с помощью git
```
git clone https://gitlab.com/driver294/cdillatest.git
```

И запустить команду
``` 
composer install
```



Настройка
-------------

### База данных

Отредактируйте файлы `config/db.php`, `config/test_db.php` в соответствии с реальными данными:

Например `db.php`
```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=test',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
```
`test_db.php`
```
$db['dsn'] = 'mysql:host=localhost;dbname=test';
```

После настроек вышеперечисленных файлов, необходимо применить миграции командой
```
yii migrate
```

Если доступы к БД заданы верно, то механизм миграций создаст 4 таблицы.
`migration`, `seans`, `ticket`, `user` . 


Доступы
-------
По умолчанию создаётся пользователь с логином: `client` и паролем: `client`

Тестирование
-------

Тесты находятся в директории `tests` . 

- `unit`  - Юнит-тесты
- `functional` - Функциональные тесты
- `acceptance` - Приемочные тесты

Юнит тесты могут быть запущены командой

```
vendor/bin/codecept run unit
```
