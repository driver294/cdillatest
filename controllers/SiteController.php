<?php

namespace app\controllers;

use app\models\Seans;
use app\models\Ticket;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /** Action для вывода расписания
     * @return string
     */
    public function actionTimetable(){

        $dataProvider = Seans::getTimetableData();

        return $this->render("timetable",[
            "dataProvider" => $dataProvider,
        ]);
    }

    /**
     * Action для покупки билетов
     * @param $id
     * @return string
     */
    public function actionBuyTicket($id){
        $seans = Seans::find()->where(["seans_id" => $id])->one();
        if(!isset($seans)){
            throw new BadRequestHttpException("Неверный ID сеанса.");
        }

        $ticket = new Ticket();
        if($ticket->load(Yii::$app->request->post())){
            $ticket->user_id = Yii::$app->user->id;
            $ticket->seans_id = $id;
            if($ticket->save()){
                return $this->redirect(Url::toRoute('site/timetable'));
            }
        }

        return $this->render("buy", [
            "seans" => $seans,
            "ticket" => $ticket,
        ]);
    }

    /**
     * Action для добавления новый сеансов
     * @return string
     */
    public function actionAddSeans(){
        $seans = new Seans();

        if($seans->load(Yii::$app->request->post())){
            if($seans->save()){
                return $this->redirect(Url::toRoute('site/timetable'));
            }
        }

        return $this->render("add-seans", [
            "seans" => $seans
        ]);
    }
}
