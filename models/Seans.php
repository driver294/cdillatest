<?php
/**
 * Created by PhpStorm.
 * User: maxweb
 * Date: 28.08.2018
 * Time: 12:03
 */
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * Модель для таблицы seans
 */
class Seans extends \yii\db\ActiveRecord
{

    public function rules()
    {
        return [
            [['seans_name', 'seans_showing_time', 'seans_showing_time'], 'required'],
            [['seans_seats'], 'integer'],
            [['seans_name'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'seans_id' => '#',
            'seans_name' => 'Название фильма',
            'seans_showing_time' => 'Время начала',
            'seans_seats' => 'Максимальное количество мест',
            'leftSeats' => 'Осталось мест',
        ];
    }


    public function getTickets()
    {
        return $this->hasMany(Ticket::class, ['seans_id' => 'seans_id']);
    }


    /**
     * Метод подготовки провайдера данных с постраничной разбивкой
     * @return ActiveDataProvider
     */
    public static function getTimetableData(){
        $query = static::find()->where([">", "seans_showing_time", new Expression('NOW()')]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'seans_showing_time' => SORT_ASC
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Метод возвращает количество оставшихся мест на сеанс
     * @return int
     */
    public function getLeftSeats(){
        $purchased = Ticket::find()->where(["seans_id" => $this->seans_id])->sum("ticket_amount");
        return $this->seans_seats - $purchased;
    }
}
