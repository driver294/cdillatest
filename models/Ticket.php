<?php
/**
 * Created by PhpStorm.
 * User: maxweb
 * Date: 28.08.2018
 * Time: 12:09
 */
namespace app\models;

use Yii;

/**
 * Модель для таблицы ticket
 */
class Ticket extends \yii\db\ActiveRecord
{

    public function rules()
    {
        return [
            [['seans_id', 'user_id', 'ticket_amount'], 'required'],
            [['seans_id', 'user_id'], 'integer'],
            [['ticket_amount'], 'integer'],
            [['ticket_amount'], 'checkAmount'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'ticket_id' => 'ID',
            'seans_id' => '# сеанса',
            'user_id' => '# пользователя',
            'ticket_amount' => 'Количество билетов',
        ];
    }

    public function checkAmount($attribute, $params){
        $seans = Seans::find()->where(["seans_id" => $this->seans_id])->one();
        if($this->ticket_amount > $seans->leftSeats)
            $this->addError($attribute, "Превышено допустимое количество мест");
    }

    public function getSeans()
    {
        return $this->hasOne(Seans::class, ['seans_id' => 'seans_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'user_id']);
    }
}
