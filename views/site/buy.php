<?php
/**
 * Created by PhpStorm.
 * User: maxweb
 * Date: 28.08.2018
 * Time: 13:22
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<h1>Купить билеты</h1>
<?php
//Этот кусок пишет ошибки валидации если они есть
if(!empty($ticket->getErrors())) {
    echo '<div class="col-md-12 alert alert-danger">';
    foreach ($ticket->getErrors() as $k => $element) {
        echo $element[0] . "<br>";
    }
    echo '</div>';
}
?>

<div class="col-md-4">
    <b>Название фильма:</b> <?=$seans->seans_name?><br>
    <b>Время сеанса:</b> <?=\Yii::$app->formatter->asDatetime($seans->seans_showing_time)?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($ticket, 'ticket_amount')->textInput(['type' => 'number', 'value' => 1]) ?>
    <div class="form-group">
        <?= Html::submitButton('Купить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
