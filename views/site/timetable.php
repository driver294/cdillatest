<?php
/**
 * Created by PhpStorm.
 * User: maxweb
 * Date: 28.08.2018
 * Time: 12:49
 */

$this->title = 'Список ближайших сеансов';
$this->params['breadcrumbs'][] = "Расписание сеансов";
?>
<h1>Расписание сеансов</h1>
<?php

try {
    echo yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => 'Нет ближайших сеансов',
        'columns' => [
            'seans_id',
            'seans_name',
            [
                "attribute" => "seans_showing_time",
                "value" => function($model){
                    return \Yii::$app->formatter->asDatetime($model->seans_showing_time);
                }
            ],
            'leftSeats',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{buy}',
                'buttons' => [
                    'buy' => function ($url, $model) {
                        return \yii\helpers\Html::a('<span class="glyphicon glyphicon-film"> Купить</span>',$url,
                            [
                                //если количество мест <1, кнопка отключена
                                "class" => "btn btn-primary ".(($model->leftSeats < 1) ? 'disabled' : '')
                            ]);
                    }
                ],
                'urlCreator' => function($action, $model, $key, $index){
                    if($action === 'buy'){
                        return \yii\helpers\Url::toRoute(['site/buy-ticket', 'id' => $model->seans_id]);
                    }
                    return false;
                }
            ]
        ]
    ]);
} catch (Exception $e) {
    echo "Ошибка";
}
