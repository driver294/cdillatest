<?php
/**
 * Created by PhpStorm.
 * User: maxweb
 * Date: 28.08.2018
 * Time: 14:00
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\datetime\DateTimePicker;
?>
<h1>Добавить новый сеанс</h1>
<?php
if(!empty($seans->getErrors())) {
    echo '<div class="col-md-12 alert alert-danger">';
    foreach ($seans->getErrors() as $k => $element) {
        echo $element[0] . "<br>";
    }
    echo '</div>';
}
?>

<div class="col-md-4">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($seans, 'seans_name')?>
    <?= $form->field($seans, 'seans_showing_time')->widget(DateTimePicker::class, [
        'options' => ['placeholder' => 'Выберите дату и время'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]);?>
    <?= $form->field($seans, 'seans_seats')->textInput(['type' => 'number', 'value' => 50]) ?>
    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
